# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-

from guardian import GuardState
import time

prefix = 'PSL'

request = 'READY_FOR_MC_LOCK'
nominal = 'IDLE'


class INIT(GuardState):
    def run(self):
        return True


class FSS_UNLOCKED(GuardState):
    def main(self):
        self.timer['wait'] = 2

    def run(self):
        if self.timer['wait']:
            if not ezca['PMC_RESONANT']:
                log('PMC not locked')
            elif not ezca['FSS_RESONANT']:
                log('FSS not locked')
            else:
                return True


class NOT_READY_FOR_MC_LOCK(GuardState):
    def run(self):
        if not ezca['PMC_RESONANT'] or not ezca['FSS_RESONANT']:
            return 'FSS_UNLOCKED'
        elif ezca['FSS_AUTOLOCK_STATE'] == 4 and ezca['FSS_TEMP_LOOP_ON']:
            log('Good to go!')
            return True
        else:
            log('Waiting for FSS...')


class READY_FOR_MC_LOCK(GuardState):
    def main(self):
        self.counter = 0

    def run(self):
        if not ezca['PMC_RESONANT'] or not ezca['FSS_RESONANT']:
            return 'FSS_UNLOCKED'
        if ezca['FSS_OSCILLATION']:
            self.counter += 1
            time.sleep(0.1)
            if self.counter >= 10:
                return 'FSS_OSCILLATING'
        else:
            return True


class FSS_OSCILLATING(GuardState):
    """If the FSS PZT FAST MON is oscillating, or 'ringing',
    come to this state and deal with it by turning down the gains.
    If we are not resonant, send back to FSS_UNLOCKED
    """
    redirect = False

    def main(self):
        # avoid hard coding these gains
        # self.low_gain = -10.0
        # self.high_gain = 20.0
        # self.step_size = 1.0
        self.original_gain = round(ezca['FSS_COMMON_GAIN'])
        self.original_fast = round(ezca['FSS_FAST_GAIN'])

        ezca['FSS_COMMON_GAIN'] = -10
        ezca['FSS_FAST_GAIN'] = -10

        self.timer['timer'] = 3

        return

    def run(self):
        if self.timer['timer']:
            # If the FSS is not resonant, do not yank the gains around
            if not ezca['FSS_RESONANT']:
                ezca['FSS_COMMON_GAIN'] = self.original_gain
                ezca['FSS_FAST_GAIN'] = self.original_fast
                return 'FSS_UNLOCKED'

            # Ramp the gains
            # if not ezca['FSS_OSCILLATION']:
            if ezca['FSS_COMMON_GAIN'] < self.original_gain:
                ezca['FSS_COMMON_GAIN'] = round(ezca['FSS_COMMON_GAIN']) + 1
            elif ezca['FSS_FAST_GAIN'] < self.original_fast:
                ezca['FSS_FAST_GAIN'] = round(ezca['FSS_FAST_GAIN']) + 1
            # else:
            #     log('seems all better; returning true')
            #     return True
            self.timer['timer'] = 0.1

            # Whether we are not oscillating or not,
            # if the common and fast gains are the same as before
            # return True and allow the state to return to 'NOT_READY_FOR_MC_LOCK'
            if (self.original_gain <= ezca['FSS_COMMON_GAIN'] and self.original_fast <= ezca['FSS_FAST_GAIN']):
                log('seems all better; returning true')
                return 'NOT_READY_FOR_MC_LOCK'


class IDLE(GuardState):
    def run(self):
        return True


edges = [
    ('FSS_UNLOCKED', 'NOT_READY_FOR_MC_LOCK'),
    ('NOT_READY_FOR_MC_LOCK', 'READY_FOR_MC_LOCK'),
    ('READY_FOR_MC_LOCK', 'NOT_READY_FOR_MC_LOCK'),
    ('FSS_OSCILLATING', 'NOT_READY_FOR_MC_LOCK'),
    ('IDLE', 'NOT_READY_FOR_MC_LOCK'),
    ('READY_FOR_MC_LOCK', 'IDLE'),
    ]

##################################################
# SVN $Id$
# $HeadURL$
##################################################
